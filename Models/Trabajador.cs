﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace prueba.Models
{
    [Table("Trabajador")]
    public class Trabajador
    {
        public int TrabajadorId { get; set; }

        [Required(ErrorMessage = "Nombre es requerido")]
        [StringLength(400, ErrorMessage = "Nombre no puede superar los 400 caracteres")]
        public string Nombre { get; set; }

        [ForeignKey(nameof(Dependencia))]
        public int DependenciaId { get; set; }
        public Dependencia Dependencia { get; set; }
    }
}