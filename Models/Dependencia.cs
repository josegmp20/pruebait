﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace prueba.Models
{
    [Table("Dependencia")]
    public class Dependencia
    {
        public int DependenciaId { get; set; }

        [Required(ErrorMessage = "Nombre es requerido")]
        [StringLength(60, ErrorMessage = "Nombre no puede superar los 60 caracteres")]
        public string Nombre { get; set; }

        public ICollection<Trabajador> Trabajadores  { get; set; }
    }
}