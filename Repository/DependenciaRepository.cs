﻿using prueba.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace prueba.Repository
{
    public class DependenciaRepository  : RepositoryBase<Dependencia>,IDependenciaRepository
    {
  
            public DependenciaRepository(RepositoryContext repositoryContext):base(repositoryContext)
            {
            }
            public IEnumerable<Dependencia> GetAllDependencias()
            {
                return FindAll()
                    .OrderBy(ow => ow.Nombre)
                    .ToList();
            }

         


      
    }
}
