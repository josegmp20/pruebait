﻿using prueba.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace prueba.Repository
{
    public interface IDependenciaRepository : IRepositoryBase<Dependencia>
    {
        IEnumerable<Dependencia> GetAllDependencias();

    }
}
