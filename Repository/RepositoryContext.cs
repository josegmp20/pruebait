﻿using prueba.Models;
using Microsoft.EntityFrameworkCore;
namespace prueba
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options)
            : base(options)
        {
        }
        public DbSet<Dependencia> Dependencias { get; set; }
        public DbSet<Trabajador> Trabajadores { get; set; }
    }
}
