﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using prueba.Models;
using prueba.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace prueba.Controllers 
{
    [ApiController]
    public class DependenciaController : ControllerBase
    {
        private readonly IDependenciaRepository _repository;

        public DependenciaController(IDependenciaRepository repository)
        {
            _repository = repository;
        }
        // GET: Dependencia
        [HttpGet("listar")]
        public IActionResult GetAll()
        {
            return Ok(_repository.GetAllDependencias());
        }






    }
}
